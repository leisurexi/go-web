package main

import (
	"gee"
	"net/http"
)

func main() {
	r := gee.New()
	r.Use(gee.Logger())
	r.Use(gee.Recovery())
	r.GET("/", func(c *gee.Context) {
		c.String(http.StatusOK, "Hello Geektutu\n")
	})

	r.GET("/panic", func(c *gee.Context) {
		names := []string{"leisurexi"}
		c.String(http.StatusOK, names[100])
	})

	r.Run(":9999")
}
